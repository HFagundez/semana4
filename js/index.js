$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    });
    $('#contacto').on('show.bs.modal', function (e) {
        console.log('***-> modal show');
        console.log(e);
        $('#contactoBtn').removeClass('btn-contacto');
        $('#contactoBtn').addClass('btn-reservado');
        $('#contactoBtn').prop('disabled', true);

    });
    $('#contacto').on('shown.bs.modal', function (e) {
        console.log('***-> modal shown');
        console.log(e);
    });
    $('#contacto').on('hide.bs.modal', function (e) {
        console.log('***-> modal hide');
        console.log(e);
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('***-> modal hidden');
        console.log(e);
        $('#contactoBtn').prop('disabled', false);
        $('#contactoBtn').addClass('btn-contacto');
    });
}); 