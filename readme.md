
Curso: Diseñando páginas web con Bootstrap 4
Entrega Semana 4

* Cargar los módulos correspondientes ejecutando: npm install
* Iniciar lite-server ejecutando: npm run dev

* El modal se activa con click en el botón Contacto del último hotel (Hotel Atlántico),
se muestran eventos del modal por consola y el botón se deshabilita y cambia de color.  

* El script npm que compila los archivos sass se llama "scss" y 
se ejecuta con: npm run scss

* En el archivo styles.scss están definidas las variables de colores requeridas y 
  otras que definen los colores de fondo del jumbotron los botones, etc y un mixin
  con valores de márgenes.

* Archivos Gruntfile.js y gulpfile.js

* El build con grunt genera salida a producción en la carpeta distgrunt.

* El build con gulp genera salida a producción en la carpeta dist.


hfagundez@protonmail.com